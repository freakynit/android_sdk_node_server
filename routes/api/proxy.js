var ENABLE_UPLOADING = true;
var ENABLE_EVENT_STATS_LOGGING = true;

var MONGO_EVENTS_DATABASE = "appengage";
var MONGO_EVENTS_TABLE = "events";

var MYSQL_EVENT_STATS_DATABASE = "appengage";
var MYSQL_EVENT_STATS_TABLE = "events_stats";

var mysql   = require("mysql");
var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var LZString = require('lz-string/libs/lz-string.js');
var request = require('request');

var db = mongojs(MONGO_EVENTS_DATABASE);
var colEvents = db.collection(MONGO_EVENTS_TABLE);

var site1 = 'http://ec2-54-190-151-212.us-west-2.compute.amazonaws.com:4444/nitin/events';
var site2 = 'http://ec2-54-190-151-212.us-west-2.compute.amazonaws.com:3333/api/events';
var site3 = 'http://c.webengage.com/m1.jpg';
var site4 = 'http://ec2-54-190-151-212.us-west-2.compute.amazonaws.com:5555/nitin2/events';

var default_headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux i686; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-us,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
    // 'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0'
};

if(ENABLE_EVENT_STATS_LOGGING == true) {
    var connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'root',
      password : 'admin123A@',
      database : MYSQL_EVENT_STATS_DATABASE
    });

    connection.connect(function(err) {
      if (err) {
        console.error('error connecting to mysql: ' + err.stack);
        return;
      }

      console.log('connected to mysql as id ' + connection.threadId);
      var sql = "";

        sql = "CREATE TABLE IF NOT EXISTS `" + MYSQL_EVENT_STATS_TABLE + "` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`licence_code` varchar(255) DEFAULT NULL,`event_name` varchar(255) DEFAULT NULL,`event_count` int(11) unsigned DEFAULT '0',`last_modified_tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        connection.query(sql, function(err, rows, fields) {
          if (err) {
            console.log("Error creating table: " + MYSQL_EVENT_STATS_TABLE);
            throw err;
            exit(1);
          } else {

          }
        });
    });
}

var uploadEvents = function(uploadData, cb) {
    request({
        url: site3,
        method: 'POST',
        form: {"data": uploadData}
        // body: JSON.stringify({"data": uploadData})
    }, function(err, res, body) {
        cb(err, res, body);
    });
}

var updateStatsInDb = function(licenceCode, eventName, totalCount, cb){
    if(licenceCode.indexOf("~~") > -1) {
        licenceCode = licenceCode.substring(1);
    }

    console.log("licenceCode, eventName, totalCount = " + licenceCode + ", " + eventName + ", " + totalCount);
    var sql = "";

    sql = "SELECT * from " + MYSQL_EVENT_STATS_TABLE + " where licence_code='" + licenceCode + "' and event_name='" + eventName + "'";
    console.log("sql = " + sql);
    connection.query(sql, function(err, rows, fields) {
      if (err) {
        cb(err, null);
      } else {
        if(rows.length < 1) {
            sql = "insert into " + MYSQL_EVENT_STATS_TABLE + " (licence_code, event_name, event_count) values('" + licenceCode + "'" + ", '" + eventName + "'" + ", 1" + ")";
            console.log("sql = " + sql);
            connection.query(sql, function(err, result){
                if (err) {
                    cb(err, null);
                } else {
                    cb(null, result)
                }
            });
        } else {
            sql = "update " + MYSQL_EVENT_STATS_TABLE + " set event_count = event_count  + " + totalCount + " where licence_code='" + licenceCode + "' and event_name='" + eventName + "'";
            console.log("sql = " + sql);
            connection.query(sql, function(err, result){
                if (err) {
                    cb(err, null);
                } else {
                    cb(null, result)
                }
            });
        }
      }
    });
}

var calculateStats = function(docs){
    var numEvents = docs.length;
    var distinctEventsCount = {};
    var numDistinctEvents = 0;

    for(var i = 0; i < numEvents; i++){
        var curDoc = docs[i];

        if(typeof distinctEventsCount[curDoc['event_name']] !== "undefined") {
            distinctEventsCount[curDoc['event_name']]++;
        } else {
            numDistinctEvents++;
            distinctEventsCount[curDoc['event_name']] = 1;
        }
    }

    return {
        "numDistinctEvents": numDistinctEvents,
        "distinctEventsCount": distinctEventsCount
    };
}

router.handleEventsData = function(req, res){
    var rawData = req.rawBody;
    //console.log("\n\n\nrawData = " + rawData + "\n\n\n");
    //console.log("\n\n\n@@@@@\neRaw payload = \n", rawData);
    
    try {
        var dataOnly = rawData.substring("data=".length);
        //console.log("\n\n\ndataOnly = " + dataOnly + "\n\n\n");

        var decoded = decodeURIComponent(dataOnly);
        //console.log("\n\n\n---------decoded data: --------- \n" + decoded + "\n\n\n");

        var decompressed = LZString.decompressFromBase64(decoded);
        //console.log("\n\n\ndecompressed = " + decompressed + "\n\n\n");

        var eData = JSON.parse(decompressed);
        //console.log("\n\n\n@@@@@\nDecrypted payload = \n", eData);
        
        var bulk = colEvents.initializeOrderedBulkOp();
        eData.forEach(function(el, index){
            bulk.insert(el);
        });
        bulk.execute(function(err, res) {
          //console.log('@@@Done!');
        });
        //colEvents.save(eData);

        var doEventStats = function(cb){
            var updateNextEventStatsToDb = function(idx, _cb){
                if(idx < eData.length) {
                    updateStatsInDb(eData[idx]['license_code'], eData[idx]['event_name'], 1, function(err, result){
                        if(err) {
                            console.log("Error in updating event stats data for licenceCode / event: " + eData[idx]['license_code'] + " / " + eData[idx]['event_name']);
                        }
                        
                        updateNextEventStatsToDb(idx + 1, _cb);
                    });
                } else {
                    _cb();
                }
            }

            updateNextEventStatsToDb(0, cb);
        }

        if (ENABLE_UPLOADING == true) {
            uploadEvents(decoded, function(err, response, body) {
                if(ENABLE_EVENT_STATS_LOGGING == true) {
                    doEventStats(function(){
                        if (!err && response.statusCode == 200) {
                            console.log("success");
                            res.json(body);
                        } else {
                            console.log("Error: ", err);
                            res.json(body);
                        }
                    });
                }
            });
        } else {
            if(ENABLE_EVENT_STATS_LOGGING == true) {
                doEventStats(function(){
                    console.log("success");
                    res.json({"success": true});
                });
            }
        }
    } catch(e){
    	console.log("error: ", e);
        res.json({"success": false});
    }
}

module.exports.router = router;
