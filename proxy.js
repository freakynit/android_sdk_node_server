var PORT = 3333;

var express = require('express');
var bodyParser = require('body-parser');
var url = require('url');

var app = express();

app.use(function(req, res, next) {
    if(req.url.indexOf("/proxy") > -1) {
        req.rawBody = '';
        req.on('data', function(chunk) {
            req.rawBody += chunk;
        });
        req.on('end', function() {
            require('./routes/api/proxy').router.handleEventsData(req, res);
        });
    } else {
        next();
    }
});

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

var router = express.Router();

app.use('/api/proxy', require('./routes/api/proxy').router);

var server = app.listen(PORT, function() {
    console.log("Listening to port %s", server.address().port);
});
